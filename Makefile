# Define variables
PROJECT_NAME = superklipper
BUILD_DIR = build
INSTALL_DIR = /usr/local/bin  # Change this to your desired install location

# Default target
.PHONY: all
all: help

# Help target
.PHONY: help
help:
	@echo "Makefile for $(PROJECT_NAME)"
	@echo "Available targets:"
	@echo "  build     - Build the project"
	@echo "  clean     - Clean the build directory"
	@echo "  install   - Install the executable"
	@echo "  help      - Show this help message"

# Build target
.PHONY: build
build:
	@echo "Building $(PROJECT_NAME)..."
	mkdir -p $(BUILD_DIR)
	cd $(BUILD_DIR) && cmake .. && cmake --build .

# Clean target
.PHONY: clean
clean:
	@echo "Cleaning build directory..."
	rm -rf $(BUILD_DIR)

# Install target
.PHONY: install
install: build
	@echo "Installing $(PROJECT_NAME)..."
	cp $(BUILD_DIR)/bin/$(PROJECT_NAME) $(INSTALL_DIR)

# Uninstall target (optional)
.PHONY: uninstall
uninstall:
	@echo "Uninstalling $(PROJECT_NAME)..."
	rm -f $(INSTALL_DIR)/$(PROJECT_NAME)

#include "FileManager.h"

FileManager::FileManager(QObject *parent) :
	QObject(parent)//,
	//m_process(new QProcess(this))
{
}

QString FileManager::read(const QString &filename)
{
	QFile file(filename);
	if(!file.open(QIODevice::ReadOnly)) {
		QMessageBox::information(0, QStringLiteral("error"), file.errorString());
	}

	QTextStream in(&file);

	//while(!in.atEnd()) {
	QString content = in.readAll();
	//}

	file.close();
	return content;
}

QString FileManager::getMimeType(const QString &filename)
{
	QMimeDatabase mdb;
	return mdb.mimeTypeForFile(QFileInfo(filename)).name();
}

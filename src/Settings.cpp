#include "Settings.h"

Settings::Settings(QObject *parent) :
	QObject(parent)
{

}


void Settings::setGroup(const QString &newgroup){
	while(group() != QStringLiteral("")){
		m_settings->endGroup();
	}
	m_settings->beginGroup(newgroup);
}
QString Settings::group(){
	return m_settings->group();
}
void Settings::init(const QString &organization, const QString &application){
	m_settings = new QSettings(organization, application);
}
QString Settings::filename(){
	return m_settings->fileName();
}
QVariant Settings::value(const QString &key, const QVariant &defaultValue){
	return m_settings->value(key, defaultValue);
}
void Settings::setValue(const QString &key, const QVariant &value){
	m_settings->setValue(key, value);
}
QStringList Settings::allKeys(){
	return m_settings->allKeys();
}
void Settings::sync(){
	m_settings->sync();
}

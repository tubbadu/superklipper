#ifndef KLIPBOARD_H
#define KLIPBOARD_H

#include <QObject>
#include <QApplication>
#include <QSettings>
#include <QImage>
#include <QList>
#include <QFile>
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>
#include <QMimeData>
#include <QClipboard>
#include <QFileInfo>
#include <QProcess>

#include <SingleApplication>
//#include <ksystemclipboard.h>
#include <KSystemClipboard>

enum dataType { PlainText=0, Image=1, Urls=2 };

class Klipboard : public QObject
{
	Q_OBJECT
public:
	explicit Klipboard(QObject *parent = 0);
	void onDataChanged(QClipboard::Mode mode);
	Q_INVOKABLE void addNewItem(QString id, dataType type, QString text, QString imagepath, QList<QUrl> urls);
	Q_INVOKABLE void pin(const QString &id);
	Q_INVOKABLE void unpin(const QString &id);
	Q_INVOKABLE void addNewTextItem(QString id, QString text);
	Q_INVOKABLE void addNewImageItem(QString id, QString image);
	Q_INVOKABLE void addNewUrlItem(QString id, QList<QUrl> urls);
	Q_INVOKABLE QVariantMap readEntry(const QString &id, const QString &group);
	Q_INVOKABLE void removeEntry(const QString &id, const QString &group);
	Q_INVOKABLE QVariantList readAllEntries(const QString &group);
	Q_INVOKABLE void trigger(const QString &id, const QString &group);
	Q_INVOKABLE QString configPath();
	Q_INVOKABLE void triggerPasteShortcut();
	void startListeningDaemon();
	void removeOlderEntries();



private:
	void endAllGroups();
	QVariant QListQUrl_to_QVariant(QList<QUrl> in);
	QList<QUrl> QVariant_to_QListQUrl(QVariant in);
	QStringList QListQUrl_to_QStringList(QList<QUrl> in);
	bool areIdentical(QVariantMap &e1, QVariantMap &e2);


	KSystemClipboard *m_clipboard;
	QSettings *m_settings;
	QList<QJsonObject> *m_items;
	QString m_configFolder;
	int m_maxentries = 50;
};

#endif

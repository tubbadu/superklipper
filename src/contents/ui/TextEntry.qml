import QtQuick 2.15
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami


ColumnLayout {
	width: parent.width
	spacing: 0
	
	function replaceUnprintableChars(txt){
		return txt
			.replace(/\&/g, "&amp;")
			.replace(/\</g, '&lt;')
			.replace(/\>/g, '&gt;')
			.replace(/\"/g, '&quot;')
			.replace(/\'/g, '&apos;')
			.replace(/\n/g, (match, endl) => `<font color='${Kirigami.Theme.focusColor}'>↵</font><br>`)
			.replace(/^( +)/gm, (match, spaces) => `<font color='${Kirigami.Theme.focusColor}'>${'⎵'.repeat(spaces.length)}</font>`)
			.replace(/(\t+)/g, (match, tabs) => `<font color='${Kirigami.Theme.focusColor}'>${'→'.repeat(tabs.length)}</font>`)
	}
	Label {
		id: label
		text: replaceUnprintableChars(args.text)
		wrapMode: Text.Wrap
		textFormat: Text.StyledText
		Layout.preferredWidth: parent.width
		//Layout.maximumHeight: 300
		elide: Label.ElideRight
		maximumLineCount: 20
	}
	Label{
		text: `<font color='${Kirigami.Theme.focusColor}'>...</font><br>`
		visible: label.truncated
	}
}

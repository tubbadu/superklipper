import QtQuick 
import QtQuick.Controls 
import QtQuick.Dialogs 
import QtQuick.Layouts 
import org.kde.kirigami as Kirigami
import org.kde.plasma.extras as PlasmaExtras
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents3
import Klipboard 

ListView {
	id: listview

	enum DataType { PlainText=0, Image=1, Urls=2 }

	highlight: PlasmaExtras.Highlight { 
		anchors.right: parent.right
		anchors.left: parent.left
	}
	highlightFollowsCurrentItem: true
	highlightMoveDuration: 10
	highlightResizeDuration: 10
	keyNavigationEnabled: true // TODO remove this and manually implement arrows up down to switch between elements, disabling the hover focus during the process


	function append(args){
		listmodel.sourceModel.append({"args": args});
	}
	function remove(args){
		for(let i=0; i<listmodel.sourceModel.count; i++){
			if(listmodel.sourceModel.get(i).args.id == args.id){
				listmodel.sourceModel.remove(i)
				return
			}
		}
	}
	function setCurrentIndex(i){
		listview.currentIndex = i;
	}
	function trigger(){
		klipboard.trigger(listmodel.get(currentIndex).args.id, "history")
		window.hide()
		if(window.pasteAfterTrigger) klipboard.triggerPasteShortcut();
	}

	model: PlasmaCore.SortFilterModel {
		id: listmodel
		filterRegExp: searchField.text.toLowerCase()
		filterCallback: (i) => {
			let args = sourceModel.get(i).args
			let searchKey = (args.text + ", " + args.urls).toLowerCase()
			return (searchKey.search(filterRegExp) >= 0)
		}

		sourceModel: ListModel {

		}
	}

	delegate: EntryDelegate{}
}

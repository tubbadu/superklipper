// Includes relevant modules used by the QML
import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.plasma.extras as PlasmaExtras
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents3


import Klipboard
import FileManager

Kirigami.ApplicationWindow {
	// ID provides unique identifier to reference this element
	id: window


	// Window title
	// i18nc is useful for adding context for translators, also lets strings be changed for different languages
	title: i18nc("@title:window", "SuperKlipper") // TODO replace everything with this
	
	property bool pasteAfterTrigger: !true

	Klipboard {
		id: klipboard
		Component.onCompleted: {
			let entries = readAllEntries("history");
			for(let i=0; i<entries.length; i++){
				klv_history.append(entries[i])
			}
			
			entries = readAllEntries("pinned");
			for(let i=0; i<entries.length; i++){
				klv_pinned.append(entries[i])
			}
		}
	}

	FileManager {
		id: filemanager
	}
	
	EntryWindow{
		id: entrywindow
		height: window.height * 0.85
		width: window.width * 0.85
	}


	RowLayout{
		width: parent.width - Kirigami.Units.gridUnit
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.top: parent.top
		anchors.topMargin: Kirigami.Units.gridUnit / 4
		
		SearchField {
			id: searchField
			Layout.fillHeight: true
			Layout.fillWidth: true
		}
		
		ToolButton {
			enabled: window.pasteAfterTrigger
			icon.name: "edit-paste"
		}
		
		ToolButton {
			icon.name: "application-menu"
		}
	}
	

	// Initial page to be loaded on app load
	pageStack.initialPage: Kirigami.Page {
		id: page
		
		//property alias klv: stackview.currentItem
		property var historyTab: KlipboardListViewHistory{
			id: klv_history
		}
		
		property var pinnedTab: KlipboardListViewPinned{
			id: klv_pinned
		}
		
		StackView{
			id: stackview
			anchors.fill: parent
			initialItem: page.historyTab
			property var pages: [page.historyTab, page.pinnedTab]
			property int pageIndex: 0
			
			function previous(){
				pageIndex--
				if(pageIndex < 0) pageIndex = pages.length-1
				if(pageIndex > pages.length-1) pageIndex = 0
				//replaceEnter = popEnter
				//replaceExit = popEnter
				replace(pages[pageIndex])
			}
			
			function next(){
				pageIndex++
				if(pageIndex < 0) pageIndex = pages.length-1
				if(pageIndex > pages.length-1) pageIndex = 0
				//replaceEnter = pushEnter
				//replaceExit = pushEnter
				replace(pages[pageIndex])
			}
		}
	}


	Component.onCompleted: {
		searchField.forceActiveFocus()
	}
	
	onActiveChanged: {
		if(!active){
			Qt.quit()
		}
	}
	
	// TODO implement settings!
}

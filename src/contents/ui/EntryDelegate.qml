import QtQuick 
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.plasma.extras  as PlasmaExtras
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents3


Item {
	height: loader.item.height + Kirigami.Units.gridUnit
	width: loader.item.width + Kirigami.Units.gridUnit
	Loader{
		id: loader
		width: listview.width - 4 // 2px of margins
		anchors.centerIn: parent
		Component.onCompleted: {
			if(args.type == KlipboardListView.DataType.PlainText){
				setSource("TextEntry.qml", {"args": args})
			} else if(args.type == KlipboardListView.DataType.Image){
				setSource("ImageEntry.qml", {"args": args})
			} else if(args.type == KlipboardListView.DataType.Urls){
				setSource("UrlsEntry.qml", {"args": args})
			} else {
				console.warn("ERROR: invalid data type:", args.type)
			}
		}
	}
	
	MouseArea {
		hoverEnabled: true
		anchors.fill: parent
		anchors.margins: 3
		
		onEntered: {
			listview.setCurrentIndex(index)
		}
		
		onClicked: (event) => {
			klipboard.trigger(args.id, "history")
			window.hide()
			if(window.pasteAfterTrigger) klipboard.triggerPasteShortcut();
		}
	}
}
import QtQuick 
import QtQuick.Controls 
import QtQuick.Dialogs 
import QtQuick.Layouts 
import org.kde.kirigami as Kirigami


ColumnLayout {
	width: parent.width
	Image {
		Layout.preferredHeight: 10 * Kirigami.Units.gridUnit
		Layout.preferredWidth: Math.min(2*height, parent.width)
		fillMode: Image.PreserveAspectCrop

		source: "file://" + args.image

		onStatusChanged: {
			if(status == Image.Error){
				// image unavailable, use fallback image
				// no lol I'm lazy to implement this'
			}
		}
	}
}

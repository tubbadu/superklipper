import QtQuick 
import QtQuick.Controls 
import QtQuick.Dialogs 
import QtQuick.Layouts 
import org.kde.kirigami as Kirigami
import org.kde.plasma.extras as PlasmaExtras
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents3
import Klipboard

KlipboardListView {
	id: listview

	delegate: EntryDelegate{
		RowLayout{
			anchors.right: parent.right
			anchors.rightMargin: 20
			anchors.verticalCenter: parent.verticalCenter
			visible: (listview.currentIndex === index)
			
			ToolButton {
				id: pin_button
				icon.name: "edit-entry"
				
				onClicked: {
					entrywindow.showWindow("Edit item", args)
				}
			}
		}
	}
}

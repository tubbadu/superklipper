import QtQuick 
import QtQuick.Controls 
import QtQuick.Dialogs 
import QtQuick.Layouts 
import org.kde.kirigami  as Kirigami
import org.kde.plasma.extras  as PlasmaExtras
import org.kde.plasma.core as PlasmaCore
import org.kde.plasma.components as PlasmaComponents3

Kirigami.SearchField {
    focus: true
    
    autoAccept: false
    onAccepted: stackview.currentItem.trigger()

    Keys.onReturnPressed: stackview.currentItem.trigger()
    Keys.onEnterPressed: stackview.currentItem.trigger()
    
    Keys.forwardTo: [stackview.currentItem]

    Keys.onUpPressed: (event) => {
        event.accepted = false
    }

    Keys.onDownPressed: (event) => {
        event.accepted = false
    }
    
    Keys.onLeftPressed: (event) => {
        event.accepted = true;
        stackview.previous()
    }
    
    Keys.onRightPressed: (event) => {
        event.accepted = true;
        stackview.next()
    }
    
    Keys.onEscapePressed: Qt.quit()
    
    Keys.onPressed: (event)=> {
        if(event.key === Qt.Key_Shift){
            window.pasteAfterTrigger = true
        }
    }
    
    Keys.onReleased: (event)=> {
        if(event.key === Qt.Key_Shift){
            window.pasteAfterTrigger = false
        }
    }
    
    onFocusChanged: {
        if(!focus){
            forceActiveFocus()
        }
    }
}

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.13 as Kirigami


Grid {
	id: gridlayout
	property int itemWidth: (6 * Kirigami.Units.gridUnit)
	Layout.fillWidth: true
	columnSpacing: Kirigami.Units.gridUnit / 2
	
	columns: Math.floor((width) / (itemWidth + columnSpacing)) 


	Repeater {
		model: args.urls.length
		Kirigami.Icon {
			id: icon
			required property int index
			height: itemWidth
			width: height
			source: filemanager.getMimeType(args.urls[index]).replace("/", "-")

			Rectangle{
				anchors.right: parent.right
				anchors.left: parent.left
				anchors.bottom: parent.bottom
				height: width / 5
				radius: Kirigami.Units.gridUnit / 5

				color: Qt.rgba(Kirigami.Theme.backgroundColor.r, Kirigami.Theme.backgroundColor.g, Kirigami.Theme.backgroundColor.b, 0.95)
				border.width: 1
				border.color: Kirigami.Theme.alternateBackgroundColor

				Label {
					anchors.centerIn: parent
					width: parent.width
					text: args.urls[index]
					elide: Label.ElideLeft
					font: Kirigami.Theme.smallFont
				}
			}
		}
	}
}

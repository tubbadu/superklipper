import QtQuick 
import QtQuick.Controls
import QtQuick.Dialogs 
import QtQuick.Layouts 
import org.kde.kirigami as Kirigami

Kirigami.ApplicationWindow { // TODO allow also for images and urls (without editing, just saving)
	id: ew
	visible: false
		
	function showWindow(title, args, label=undefined){
		ew.title = title
		ew.args = args
		ew.text = args.text
		ew.image = args.image
		//ew.urls = args.urls
		ew.type = args.type
		
		if(label){
			ew.label = label
		} else if(args.label){
			ew.label = args.label
		} else {
			ew.label = ""
		}
		
		//loader.loadMetadata()
		show()
	}
	
	property var args: {"text":""}
	
	/*onArgsChanged: {
		entryText.text = ""
		entryImage.source = ""
		
		
		if(args.type == KlipboardListView.PlainText){
			entryText.text = args.text
		} else if(args.type == KlipboardListView.Image){
			entryImage.source = "file://" + args.image
		} else if(args.type == KlipboardListView.Urls){
			// TODO
		}
	}*/
	property alias label: entryLabel.text
	property alias text: entryText.text
	property alias image: entryImage.image
	/*property var urls //: entryImage.image*/
	property int type: -1
	
	pageStack.initialPage: Kirigami.Page {
		
		Keys.onEscapePressed: ew.hide()
		
		actions.contextualActions: [
			Kirigami.Action {
				icon.name: "document-save"
				onTriggered: {
					// remove previous entry
					klv_pinned.remove(args)
					klipboard.unpin(args.id)
					// now save the current one
					args.text = entryText.text
					args.label = entryLabel.text
					klv_pinned.append(args)
					klipboard.pin(args.id)
					ew.hide()
				}
			},
			Kirigami.Action {
				icon.name: "edit-delete"
				onTriggered: {
					klv_pinned.remove(args)
					klipboard.unpin(args.id)
					ew.hide()
				}
			}
		]
		
		ColumnLayout {
			anchors.fill: parent
			Layout.fillWidth: true
			TextField {
				id: entryLabel
				Layout.fillWidth: true
				placeholderText: "Entry label..."
				text: ew.label
			}
			
			Image { // TODO add rounded borders, if possible
				id: entryImage
				visible: (ew.type == KlipboardListView.Image)
				Layout.fillWidth: true
				//Layout.preferredHeight: ew.height
				Layout.fillHeight: true
				fillMode: Image.PreserveAspectFit
				
				property string image: ""
				
				onImageChanged: {
					if(image && image.length > 0){
						source = "file://" + image
					}
				}
			}
			
			ScrollView{
				Layout.fillWidth: true
				Layout.fillHeight: true
				visible: ((ew.type == KlipboardListView.DataType.PlainText) || (ew.type == KlipboardListView.Urls))
				
				Text {
					id: entryText
					text: "aaaaaaaaaaaaa"
				}
				/*TextArea {
					id: entryText
					visible: false //(ew.type == KlipboardListView.DataType.PlainText)
					Layout.fillWidth: true
					Layout.fillHeight: true
					textFormat: Text.PlainText
					placeholderText: "Entry text..."
					text: ew.text
				}*/
				
				Loader{
					id: loader
					visible: (ew.type == KlipboardListView.Urls)
					Layout.fillWidth: true
					Layout.fillHeight: true
					
					function loadMetadata() {
						if(args.type == KlipboardListView.DataType.Urls){
							setSource("UrlsEntry.qml", {"args": ew.args})
						}
					}
				}
			}
		}
	}
}
#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QSettings>

class Settings : public QObject
{
	Q_OBJECT
	//Q_PROPERTY(QString group READ group WRITE setGroup)
	//Q_PROPERTY(QString fileName READ filename)
public:
	explicit Settings(QObject *parent = 0);
	Q_INVOKABLE void setGroup(const QString &newgroup);
	Q_INVOKABLE QString group();
	Q_INVOKABLE void init(const QString &organization, const QString &application);
	Q_INVOKABLE QString filename();
	Q_INVOKABLE QVariant value(const QString &key, const QVariant &defaultValue);
	Q_INVOKABLE void setValue(const QString &key, const QVariant &value);
	Q_INVOKABLE QStringList allKeys();
	Q_INVOKABLE void sync();




private:
	QSettings *m_settings;
};

#endif

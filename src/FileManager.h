#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QMimeDatabase>

#include <QMimeType>
#include <QFileInfo>

class FileManager : public QObject
{
    Q_OBJECT
public:
    explicit FileManager(QObject *parent = 0);
    Q_INVOKABLE QString read(const QString &filename);
    Q_INVOKABLE QString getMimeType(const QString &filename);


private:
    //QProcess *m_process;
};

#endif

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <QCommandLineParser>
#include <QDebug>
#include <QProcess>
#include <QMimeData>
#include <QClipboard>

#include <SingleApplication>
//#include <ksystemclipboard.h>
#include <KSystemClipboard>

#include "klipboard.h"
#include "FileManager.h"
#include "Settings.h"


// TODO add QCoreApplication::translate(...)
#define parserOption1(name) parser.addOption({QStringLiteral(name)})
#define parserOption2(name, description) parser.addOption({QStringLiteral(name), QStringLiteral(description)})
#define parserOption3(name, nameshort, description) parser.addOption({{QStringLiteral(name), QStringLiteral(nameshort)}, QStringLiteral(description)})
#define parserOption4(name, nameshort, description, parameter) parser.addOption({{QStringLiteral(name), QStringLiteral(nameshort)}, QStringLiteral(description), QStringLiteral(parameter)})

#define GET_MACRO(_1, _2, _3, _4, NAME, ...) NAME
#define parserOption(...) GET_MACRO(__VA_ARGS__, parserOption4, parserOption3, parserOption2, parserOption1)(__VA_ARGS__)


#define parserIsSet(name) parser.isSet(QStringLiteral(name))
#define parserValue(name) parser.value(QStringLiteral(name))

int main(int argc, char *argv[0])
{
	SingleApplication app(argc, argv, true);

	KLocalizedString::setApplicationDomain("superklipper");
	QCoreApplication::setOrganizationName(QStringLiteral("KDE"));
	QCoreApplication::setOrganizationDomain(QStringLiteral("kde.org"));
	QCoreApplication::setApplicationName(QStringLiteral("SuperKlipper"));

	Klipboard *klipboard;


	QCommandLineParser parser;
	parser.setApplicationDescription(QStringLiteral("A Clipboard manager for KDE"));
	parser.addHelpOption();
	parser.addVersionOption();
	
	
	parserOption("d", "daemon", "Launch daemon instance.");
	parserOption("g", "gui", "Launch GUI instance.");
	parserOption("config-location", "Get config file location.");
	parserOption("t", "trigger", "Trigger clipboard entry identified by <id>.", "id");
	parserOption("p", "paste", "After triggering the specified entry, emulate a paste shortcut.");
	

	// parser.addOptions(
	// 	{
	// 		{
	// 			{"d", "daemon"},
	// 			QCoreApplication::translate("main", "Launch daemon instance.")
	// 		},
	// 		{
	// 			{"g", "gui"},
	// 			QCoreApplication::translate("main", "Launch GUI instance.")
	// 		},
	// 		{"config-location",
	// 			QCoreApplication::translate("main", "Get config file location.")
	// 		},
	// 		{
	// 			{"t", "trigger"},
	// 			QCoreApplication::translate("main", "Trigger clipboard entry identified by <id>."),
	// 			QCoreApplication::translate("main", "id")
	// 		},
	// 		{
	// 			{"p", "paste"},
	// 			QCoreApplication::translate("main", "After triggering the specified entry, emulate a paste shortcut.")
	// 		}
	// 	}
	// );
	parser.process(app);
	qWarning() << "new instance!";
	if( app.isSecondary() ) {
		qWarning() << "Secondary instance!";

		if(parserIsSet("daemon")){
			qWarning() << "Warning: daemon is already running";
			return 0; // TODO set better suited return codes
		} else if(parserIsSet("gui")){
			// run gui
			qWarning() << "Running gui.";
			QQmlApplicationEngine engine;
			qmlRegisterType<Klipboard>("Klipboard", 1, 0, "Klipboard");
			qmlRegisterType<FileManager>("FileManager", 1, 0, "FileManager");
			qmlRegisterType<Settings>("Settings", 1, 0, "Settings");
			engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
			engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
			if (engine.rootObjects().isEmpty()) {
				qWarning() << "QML crashed";
				return -1;
			}
			return app.exec();
		} else if(parserIsSet("trigger")){
			app.sendMessage(QString(QStringLiteral("trigger:") + parserValue("trigger")).toUtf8());
			return 0;
		} else if(parserIsSet("config-location")){
			qWarning() << klipboard->configPath(); // TODO print to std::out!
			return 0;
		} else {
			// if nothing has been set, run gui
			// TODO implement
		}
		// TODO implement else if with all handlings
		app.exit(0);
		return 0; // because the other one does not work lol
	}

	// primary instance
	//if(parser.optionNames().count() == 0){
	//	parser.showHelp();
	//} else 
	if(parserIsSet("gui")){
		qWarning() << "Warning: unable to run gui, because daemon is not running. Run \"superklipper --daemon\" to start it.";
	}

	if(parserIsSet("daemon") || parser.optionNames().length() == 0){
		qWarning() << "Starting daemon.";
		klipboard = new Klipboard();
		klipboard->startListeningDaemon();

		QObject::connect(
            &app,
            &SingleApplication::receivedMessage,
            &app,
            [=](int instanceId, QByteArray message){
				QString msg = QString::fromLatin1(message);
				if(!msg.contains(QStringLiteral(":"))){
					qWarning() << "Warning: unvalid message received:" << message;
				}
				QString action = msg.split(QStringLiteral(":"))[0];
				QString arg = msg.split(QStringLiteral(":"))[1];
				

				if(action == QStringLiteral("trigger")){
					//klipboard->trigger(arg);
					//TODO implement using also the group!
				}
			}
        );
		return app.exec();
	} else if(parserIsSet("config-location")){
		qWarning() << klipboard->configPath(); // TODO print to std::out!
		return 0;
	}
	return 0;


	/*if(parserIsSet("gui") || parser.optionNames().count() == 0){
		qWarning() << "Spawning gui in another instance.";
		QProcess p;
		QString program = *argv;
		QStringList args;
		args << "--gui";
		if(parserIsSet("paste")){
			args << "--paste";
		}
		p.setProgram(program);
		p.setArguments(args);
		p.startDetached();
	}*/
}


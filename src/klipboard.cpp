#include "klipboard.h"

Klipboard::Klipboard(QObject *parent) :
	QObject(parent)
{
	m_clipboard = KSystemClipboard::instance();
	m_items = new QList<QJsonObject>();
	m_settings = new QSettings(QStringLiteral("superklipper"), QStringLiteral("SuperKlipper"));


	QFileInfo fileinfo(m_settings->fileName());
	m_configFolder = fileinfo.absolutePath();
}

void Klipboard::startListeningDaemon(){
	QObject::connect(m_clipboard, &KSystemClipboard::changed, this, &Klipboard::onDataChanged);
}

QString Klipboard::configPath(){
	return m_configFolder;
}

void Klipboard::onDataChanged(QClipboard::Mode mode){
	if(mode != QClipboard::Clipboard) return; // only track clipboard

	// TODO: check if timestamp has already been used (superfast copying), and if yes change with "timestamp.1" and increasing
	const QString timestamp = QStringLiteral("t_") + QString::number(QDateTime::currentMSecsSinceEpoch()); // this will be used as id
	const QMimeData *mimedata = m_clipboard->mimeData(QClipboard::Clipboard);

	if(!mimedata){
		return; // invalid mimedata. ignore it
	}
	if(mimedata->hasFormat(QStringLiteral("application/x-kde-onlyReplaceEmpty"))){
		return; //It's a second copy. ignore it
	}

	qWarning() << "(Valid) Data changed!";

	if(mimedata->hasImage()){
		QImage img;
		QString imagefilename(m_configFolder + QStringLiteral("/") + timestamp + QStringLiteral(".png")); // extension is needed, otherwise an error is raised, dunno why
		if(img.loadFromData(mimedata->data(QStringLiteral("image/png")))){ // if it works, don't change it'
			// image retrieved successfully
			// now save it to a file
			if(img.save(imagefilename)){
				addNewImageItem(timestamp, imagefilename);
				qDebug() << "Image saved successfully";
			} else {
				qWarning() << "Error: Image saving had a problem" << imagefilename;
			}
		} else {
			qWarning() << "Error: unable to save copied image to file." << imagefilename;
		}
	} else if(mimedata->hasUrls()){
		addNewUrlItem(timestamp, mimedata->urls());
	} else if(mimedata->hasText()){
		addNewTextItem(timestamp, mimedata->text());
	} else {
		qWarning() << "Error: Data not supported"; // TODO add other formats like html and color
	}
}

void Klipboard::addNewItem(QString id, dataType type, QString text, QString image, QList<QUrl> urls){
	QVariantMap item;
	item[QStringLiteral("id")] = QVariant(id);
	item[QStringLiteral("type")] = QVariant(type);
	item[QStringLiteral("text")] = QVariant(text);
	item[QStringLiteral("image")] = QVariant(image);
	item[QStringLiteral("urls")] = QListQUrl_to_QVariant(urls);

	QVariant item_v(item); // perhaps redundant because of implicit conversion

	endAllGroups(); // just to be sure
	m_settings->beginGroup(QStringLiteral("history"));
	m_settings->setValue(id, item_v);
	m_settings->endGroup();

	qWarning() << "removing older entries";
	removeOlderEntries();
}
void Klipboard::addNewTextItem(QString id, QString text){
	addNewItem(id, PlainText, text, QString(), QList<QUrl>());
}
void Klipboard::addNewImageItem(QString id, QString image){
	addNewItem(id, Image, QString(), image, QList<QUrl>());
}
void Klipboard::addNewUrlItem(QString id, QList<QUrl> urls){
	addNewItem(id, Urls, QString(), QString(), urls);
}

void Klipboard::pin(const QString &id){
	QVariantMap item = readEntry(id, QStringLiteral("history"));
	const QString timestamp = QStringLiteral("t_") + QString::number(QDateTime::currentMSecsSinceEpoch()); // this will be used as id
	item[QStringLiteral("id")] = QVariant(timestamp);
	
	if(item[QStringLiteral("image")].toString().length() > 3){
		// copy the file
		QString imagefilename(m_configFolder + QStringLiteral("/") + timestamp + QStringLiteral(".png"));
		QFile(item[QStringLiteral("image")].toString()).copy(imagefilename);
		item[QStringLiteral("image")] = QVariant(imagefilename);
	}
	endAllGroups(); // just to be sure
	m_settings->beginGroup(QStringLiteral("pinned"));
	m_settings->setValue(id, item);
	m_settings->endGroup();
}

void Klipboard::unpin(const QString &id){
	//endAllGroups(); // just to be sure
	//m_settings->beginGroup("pinned");
	//m_settings->remove(id);
	removeEntry(id, QStringLiteral("pinned"));
	//m_settings->endGroup();
}

void Klipboard::removeOlderEntries(){
	endAllGroups();
	m_settings->beginGroup(QStringLiteral("history"));
	QStringList entries = m_settings->childKeys();
	entries.sort(); // not needed but it's better to make sure the entries are properly sorted
	QVariantMap firstEntry = readEntry(entries.constLast(), QStringLiteral("history"));
	for(int i = 1; i<entries.length()-1; i++){
		QVariantMap entry = readEntry(entries[i], QStringLiteral("history"));
		if(areIdentical(firstEntry, entry)) {
			qWarning() << "removing identical entry";
			//m_settings->remove(entries[i]);
			removeEntry(entries[i], QStringLiteral("history"));
		} else {
			//qWarning() << "not identical!" << firstEntry[QStringLiteral("text")] << firstEntry[QStringLiteral("type")] << entry[QStringLiteral("text")] << entry[QStringLiteral("type")];
		}
	}
	
	if(entries.length() <= m_maxentries){
		return;
	}
	//QString max_id = entries[m_maxentries-1];
	//for(QString key : entries.sliced(m_maxentries)){ // Qt6
	//for(int i = m_maxentries; i < entries.length(); i++){
	for(int i = 0; i < entries.length() - m_maxentries; i++){
		const QString key = entries[i];
		qWarning() << "removing:" << key;
		if(readEntry(key, QStringLiteral("history"))[QStringLiteral("type")].toInt() == Image){
			qWarning() << "removing also image file" << readEntry(key, QStringLiteral("history"))[QStringLiteral("image")].toString();
			QFile::remove(readEntry(key, QStringLiteral("history"))[QStringLiteral("image")].toString());
		}
		//m_settings->remove(key);
		removeEntry(key, QStringLiteral("history"));
	}
}

bool Klipboard::areIdentical(QVariantMap &e1, QVariantMap &e2){
	if(e1.value(QStringLiteral("type")) == e2.value(QStringLiteral("type"))){
		if(e1.value(QStringLiteral("type")) == PlainText){
			return e1.value(QStringLiteral("text")) == e2.value(QStringLiteral("text"));
		} else if(e1.value(QStringLiteral("type")) == Image){
			return e1.value(QStringLiteral("image")) == e2.value(QStringLiteral("image"));
		} else if(e1.value(QStringLiteral("type")) == Urls){
			return e1.value(QStringLiteral("urls")) == e2.value(QStringLiteral("urls"));
		}
	}
	
	return false;
}

void Klipboard::endAllGroups(){
	while(m_settings->group() != QStringLiteral("")){
		m_settings->endGroup();
	}
}

QVariantMap Klipboard::readEntry(const QString &id, const QString &group = QStringLiteral("history")){
	endAllGroups();
	m_settings->beginGroup(group);
	QVariant item_v = m_settings->value(id, QVariant());
	if(item_v == QVariant()) {
		qWarning() << "not present";
		return QVariantMap();
	}
	return item_v.toMap();
}

QVariantList Klipboard::readAllEntries(const QString &group){
	QVariantList entries;
	endAllGroups();
	m_settings->beginGroup(group);
	for(QString key : m_settings->childKeys()){
		entries.prepend(readEntry(key, group)); // in order to reverse the list
	}

	return entries;
}

void Klipboard::trigger(const QString &id, const QString &group){
	QProcess wlcopy;
	wlcopy.setProgram(QStringLiteral("wl-copy"));
	QVariantMap k = readEntry(id, group);
	if(k[QStringLiteral("type")].toInt() == Urls) {
		QList<QUrl> urls = QVariant_to_QListQUrl(k[QStringLiteral("urls")]);
		if(urls.isEmpty()){
			qWarning() << "ERROR: empty url list";
			return;
		}
		//mime->setUrls(urls);
		wlcopy.setArguments(QStringList() << QStringLiteral("--type") << QStringLiteral("text/uri-list") << QListQUrl_to_QStringList(urls).join(QStringLiteral("\n")));
	} else if(k[QStringLiteral("type")].toInt() == PlainText){
		//qWarning() << "setting text:" << k[QStringLiteral("text")].toString();
		//mime->setText(k[QStringLiteral("text")].toString());
		wlcopy.setArguments(QStringList() << k[QStringLiteral("text")].toString());
	} else if(k[QStringLiteral("type")].toInt() == Image){
		//QImage img(k[QStringLiteral("image")].toString());
		//if(img.isNull()){
		//	qWarning() << "ERROR: Unable to load image" << k[QStringLiteral("image")];
		//	return;
		//}
		//mime->setImageData(img);
		wlcopy.setArguments(QStringList() << QStringLiteral("--type") << QStringLiteral("image/png"));
		wlcopy.setStandardInputFile(k[QStringLiteral("image")].toString());
	} else {
		qWarning() << "ERROR: unknown type:" << k[QStringLiteral("type")];
	}
	
	// now remove selected entry
	//m_settings->remove(id);
	removeEntry(id, group);
	
	//m_clipboard->setMimeData(mime, QClipboard::Clipboard);
	wlcopy.start();
	if(wlcopy.waitForFinished()){
		qWarning() << "wl-copy run correctly";
	} else {
		qWarning() << "wl-copy run with errors!";
	}
}

void Klipboard::triggerPasteShortcut(){
	QProcess dotoolc;
	dotoolc.setProgram(QStringLiteral("bash"));
	dotoolc.setArguments(QStringList() << QStringLiteral("-c") << QStringLiteral("sleep 0.1; echo key shift+insert | dotoolc"));
	dotoolc.startDetached();
	QApplication::quit();
}

void Klipboard::removeEntry(const QString &id, const QString &group){
	QVariantMap entry = readEntry(id, group);
	if(entry[QStringLiteral("image")].toString().length() > 3){
		// also remove the image
		qWarning() << "removing image" << entry[QStringLiteral("image")].toString();
		QFile(entry[QStringLiteral("image")].toString()).remove();
	}
	
	// now remove it
	endAllGroups();
	m_settings->beginGroup(group);
	m_settings->remove(id);
}

QVariant Klipboard::QListQUrl_to_QVariant(QList<QUrl> in){
	QVariantList out;
	for( QUrl url : in){
		out.append(QVariant(url));
	}
	return QVariant(out);
}

QStringList Klipboard::QListQUrl_to_QStringList(QList<QUrl> in){
	QStringList out;
	for( QUrl url : in){
		out.append(url.toString());
	}
	return out;
}

QList<QUrl> Klipboard::QVariant_to_QListQUrl(QVariant in){
	QList<QUrl> out;
	for( QVariant url : in.toList()){
		out.append(url.toUrl());
	}
	return out;
}
